FROM golang:1.18 AS builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY *.go ./

COPY config ./config
COPY db ./db
COPY server ./server
COPY util ./util

RUN go build -o /app/runner

# run the app
FROM golang:1.18

WORKDIR /app

COPY --from=builder /app/runner .
COPY app.env ./
COPY db/migration ./db/migration

CMD ["/app/runner"]
