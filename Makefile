include app.env
export

PWD=$(dir $(realpath $(lastword $(MAKEFILE_LIST))))

up:
	docker-compose rm -f && \
	docker-compose build && \
	docker-compose --env-file ./app.env up

# use this to while developing / testing with `air`
# `air` => live reloading for golang
db_dev:
	docker run -v "$(PWD)/tmp/vol_postgresql_db_dev:/var/lib/postgresql/data" \
	--name ct_postgres --rm -p 5432:5432 \
	-e POSTGRES_USER=root -e POSTGRES_DB=pulse_id \
	-e POSTGRES_PASSWORD=secret \
	postgres:latest

db_test:
	docker run --name ct_postgres_test --rm -p 5433:5432 \
	-e POSTGRES_USER=root -e POSTGRES_DB=pulse_id_test \
	-e POSTGRES_PASSWORD=secret \
	postgres:latest

sqlc:
	sqlc generate


.PHONY: up db_dev db_test sqlc
