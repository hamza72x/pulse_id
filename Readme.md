# Env files:

• For development with [air](https://github.com/cosmtrek/air), create `app.air.env` (configurable in `.air.toml` file)

• For testing (`go test -v ./...`) create `app.test.env` (required)

• Sample env file:

```ini
GIN_MODE=debug
# the port to run on
PORT=3001

# use `service` name (service_db) while using docker-compose
DB_HOST=127.0.0.1 
DB_PORT=5432
DB_USERNAME=root
DB_PASSWORD=secret
DB_NAME=pulse_id

BASIC_AUTH_PASS=secret_password

MIGRATION_URL=file://db/migration
```
# Running:

1. Run Docker
2. Create `app.env` file
3. `make up` (Check `Makefile` for more details regarding `up` command)

If configuration files are okay, then you should be able to access the api at: localhost:3001

# API Endpoints:

• Admin endpoints uses "Basic Auth" method

1. Create token with `POST /v1/admin/tokens`

```sh
curl -L -X POST -u "admin:secret_password" -d '{"length": 10}' localhost:3001/v1/admin/tokens
```

2. List all tokens with `GET /v1/admin/tokens`

```sh
curl -L -X GET -u "admin:secret_password" localhost:3001/v1/admin/tokens
```

3. Validate a token with `POST /v1/tokens/validate`

```sh
# returns the token object if it's valid
curl -L -X POST -d '{"token": "<your_token>"}' localhost:3001/v1/tokens/validate
```

# Testing

1. Run Docker
2. Create `app.test.env` file (Make sure to set this: `MIGRATION_URL=file://../db/migration`)
3. Run test postgres container

```sh
docker run --name ct_postgres_test --rm -p 5433:5432 \
-e POSTGRES_USER=root -e POSTGRES_DB=pulse_id_test \
-e POSTGRES_PASSWORD=secret \
postgres:latest
```

4. `go test -v ./...`


# Some Note:

• Used [air](https://github.com/cosmtrek/air) for live reload while developing and ran following postgres container:

```sh
docker run -v "$(pwd)/tmp/vol_postgresql_air:/var/lib/postgresql/data" \
--name ct_postgres --rm -p 5432:5432 \
-e POSTGRES_USER=root -e POSTGRES_DB=pulse_id \
-e POSTGRES_PASSWORD=secret \
postgres:latest
```

• Used [migrate](https://github.com/golang-migrate/migrate) for database migration.

• Used [sqlc](https://github.com/kyleconroy/sqlc) for type-safe code for sql.
