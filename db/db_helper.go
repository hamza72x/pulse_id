package db

import (
	"hamza72x/pulse_id/config"
	"hamza72x/pulse_id/util"
	"log"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	col "github.com/hamza72x/go-color"
)

func RunMigration(cfg config.Config) {

	migration, err := migrate.New(cfg.MIGRATION_URL, cfg.GetDBUrl())
	util.LogFatal(err, "failed to create migration instance")

	if err = migration.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatal("failed to run up", err)
	}

	log.Println(col.Green("db migrated successfully"))
}
