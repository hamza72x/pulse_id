CREATE TABLE "tokens" (
  "id" BIGSERIAL PRIMARY KEY,
  "token" varchar UNIQUE NOT NULL,
  "expires_at" timestamp NOT NULL,
  "is_disabled" boolean NOT NULL DEFAULT (false)
);
