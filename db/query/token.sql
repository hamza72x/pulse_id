-- name: GetToken :one
SELECT * FROM tokens WHERE id = $1 LIMIT 1;

-- name: ListTokens :many
SELECT * FROM tokens ORDER BY expires_at;

-- name: CreateToken :one
INSERT INTO tokens (token, expires_at) VALUES ($1, $2) 
RETURNING *;

-- name: EnableToken :one
UPDATE tokens SET is_disabled = false WHERE id = $1
RETURNING *;

-- name: DisableToken :one
UPDATE tokens SET is_disabled = true WHERE id = $1
RETURNING *;

-- name: GetTokenByToken :one
SELECT * FROM tokens WHERE token = $1 LIMIT 1;