package db

import "database/sql"

type Store struct {
	db *sql.DB
	*Queries
}

func (s *Store) GetSqlDB() *sql.DB {
	return s.db
}

// NewStore creates a new store
func NewStore(db *sql.DB) *Store {
	return &Store{
		db:      db,
		Queries: New(db),
	}
}
