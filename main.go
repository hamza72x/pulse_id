package main

import (
	"database/sql"
	"flag"
	"hamza72x/pulse_id/config"
	"hamza72x/pulse_id/db"
	sqlc "hamza72x/pulse_id/db/sqlc"
	"hamza72x/pulse_id/server"
	"hamza72x/pulse_id/util"

	_ "github.com/lib/pq"
)

type flagArgs struct {
	Env string
}

var (
	flagArg flagArgs
)

func main() {

	parseFlags()

	// config load
	cfg, err := config.LoadConfig(flagArg.Env)
	util.LogFatal(err, "failed to load config")

	util.PrettyPrint(&cfg)

	// set database connection
	conn, err := sql.Open(config.DB_DRIVER, cfg.GetDBUrl())
	util.LogFatal(err, "failed to open db connection")

	util.PrettyPrint(conn.Stats())

	// run migration
	db.RunMigration(cfg)

	// new store
	store := sqlc.NewStore(conn)

	// new server
	server, err := server.New(cfg, store)
	util.LogFatal(err, "failed to create server")

	// start server
	err = server.Start()
	util.LogFatal(err, "failed to start server")
}

func parseFlags() {
	flag.StringVar(&flagArg.Env, "env", "app.env", "the config file name")
	flag.Parse()
}
