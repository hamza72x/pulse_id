package controllers

import (
	"context"
	"database/sql"
	db "hamza72x/pulse_id/db/sqlc"
	"hamza72x/pulse_id/server/requests"
	"hamza72x/pulse_id/util"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type TokenController struct {
	store *db.Store
}

func NewTokenController(store *db.Store) *TokenController {
	return &TokenController{
		store: store,
	}
}

// Create creates a new token
func (t *TokenController) Create(c *gin.Context) {

	var req requests.TokenCreateRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	token, err := t.generate(req.Length)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, token)
}

// Validate validates a token
// TODO: add rate limiter based on IP
func (t *TokenController) Validate(c *gin.Context) {
	var req requests.TokenValidateRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	token, err := t.store.GetTokenByToken(context.Background(), req.Token)

	if err != nil {
		if err == sql.ErrNoRows {
			c.JSON(http.StatusNotFound, gin.H{
				"error": "invalid token",
			})
			return
		}

		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	if token.ExpiresAt.Before(time.Now()) {
		c.JSON(http.StatusForbidden, gin.H{
			"error": "token expired",
		})
		return
	}

	if token.IsDisabled {
		c.JSON(http.StatusForbidden, gin.H{
			"error": "token disabled",
		})
		return
	}

	c.JSON(http.StatusOK, token)
}

// List returns all tokens
func (t *TokenController) List(c *gin.Context) {

	tokens, err := t.store.ListTokens(context.Background())

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, tokens)
}

// generate generates a new token
// also inserts it into the database
func (t *TokenController) generate(length int) (db.Token, error) {
	token := util.CreateRandomString(length)

	if _, err := t.store.GetTokenByToken(context.Background(), token); err == sql.ErrNoRows {
		return t.store.CreateToken(context.Background(), db.CreateTokenParams{
			Token:     token,
			ExpiresAt: time.Now().Add(time.Hour * 24 * 7),
		})
	}

	return t.generate(length)
}
