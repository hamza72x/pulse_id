package server

import (
	"database/sql"
	"fmt"
	"hamza72x/pulse_id/config"
	"hamza72x/pulse_id/db"
	sqlc "hamza72x/pulse_id/db/sqlc"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/require"
)

func newTestServer(t *testing.T) *Server {

	cfg, err := config.LoadConfig("../app.test.env")
	require.NoError(t, err)

	// set database connection
	conn, err := sql.Open(config.DB_DRIVER, cfg.GetDBUrl())
	require.NoError(t, err)

	db.RunMigration(cfg)

	store := sqlc.NewStore(conn)

	server, err := New(cfg, store)
	require.NoError(t, err)

	return server
}

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	os.Exit(m.Run())
}

func execRequest(t *testing.T, server *Server, req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	server.router.ServeHTTP(rr, req)
	return rr
}

func tearDown(t *testing.T, store *sqlc.Store, table string, column string, relation string, value interface{}) {
	_, err := store.GetSqlDB().Exec(
		fmt.Sprintf("DELETE FROM %s WHERE %s %s $1", table, column, relation),
		value,
	)
	require.NoError(t, err)
}
