package requests

type TokenCreateRequest struct {
	Length int `json:"length" validate:"required,min=6,max=12"`
}

type TokenValidateRequest struct {
	Token string `json:"token" validate:"required"`
}
