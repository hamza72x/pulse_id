package server

import (
	"hamza72x/pulse_id/config"
	db "hamza72x/pulse_id/db/sqlc"

	"github.com/gin-gonic/gin"
)

type Server struct {
	cfg    config.Config
	router *gin.Engine
	store  *db.Store
}

func New(cfg config.Config, store *db.Store) (*Server, error) {

	s := &Server{
		cfg:   cfg,
		store: store,
	}

	r := gin.Default()

	// token routes
	s.setTokenRoutes(r)

	// more routes

	s.router = r

	return s, nil
}

func (s *Server) Start() error {
	return s.router.Run(":" + s.cfg.PORT)
}
