package server

import (
	"hamza72x/pulse_id/server/controllers"

	"github.com/gin-gonic/gin"
)

func (s *Server) setTokenRoutes(r *gin.Engine) {

	controller := controllers.NewTokenController(s.store)

	v1 := r.Group("/v1")
	admin := v1.Group("/admin")

	// using a very basic auth
	// on postman use "Basic Auth"
	admin.Use(gin.BasicAuth(gin.Accounts{
		"admin": s.cfg.BASIC_AUTH_PASS,
	}))

	adminTokens := admin.Group("/tokens")

	adminTokens.GET("/", controller.List)
	adminTokens.POST("/", controller.Create)

	// public routes
	v1.POST("/tokens/validate", controller.Validate)
}
