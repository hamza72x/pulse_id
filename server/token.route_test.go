package server

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	db "hamza72x/pulse_id/db/sqlc"
	"hamza72x/pulse_id/util"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestTokenCreate(t *testing.T) {

	server := newTestServer(t)

	testCases := []struct {
		name          string
		buildRequest  func() *http.Request
		checkResponse func(t *testing.T, rr *httptest.ResponseRecorder) db.Token
	}{
		{
			name: "UnAuthorized",
			buildRequest: func() *http.Request {
				req, _ := http.NewRequest("POST", "/v1/admin/tokens/", nil)
				return req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder) db.Token {
				require.Equal(t, http.StatusUnauthorized, rr.Code)
				return db.Token{}
			},
		},
		{
			name: "Successfull Creation",
			buildRequest: func() *http.Request {
				req, _ := http.NewRequest("POST", "/v1/admin/tokens/",
					bytes.NewBuffer([]byte(`{"length": 10}`)),
				)
				req.SetBasicAuth("admin", server.cfg.BASIC_AUTH_PASS)
				return req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder) db.Token {
				require.Equal(t, http.StatusOK, rr.Code)

				var token db.Token

				err := json.Unmarshal(rr.Body.Bytes(), &token)
				require.NoError(t, err)

				require.NotEmpty(t, token)
				require.Equal(t, 10, len(token.Token))
				require.Greater(t, token.ID, int64(0))

				return token
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			token := tc.checkResponse(t, execRequest(t, server, tc.buildRequest()))
			tearDown(t, server.store, "tokens", "id", "=", token.ID)
		})
	}

}

func TestTokenValidation(t *testing.T) {

	server := newTestServer(t)

	testCases := []struct {
		name          string
		buildRequest  func() (db.Token, *http.Request)
		checkResponse func(t *testing.T, rr *httptest.ResponseRecorder, token db.Token)
	}{
		{
			name: "404",
			buildRequest: func() (db.Token, *http.Request) {
				req, _ := http.NewRequest("POST",
					"/v1/tokens/validate",
					bytes.NewBufferString(`{"token": "404_TOKEN"}`),
				)
				req.Header.Set("Content-Type", "application/json")
				return db.Token{}, req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder, token db.Token) {
				require.Equal(t, http.StatusNotFound, rr.Code)
			},
		},
		{
			name: "Expired Token",
			buildRequest: func() (db.Token, *http.Request) {
				token, err := server.store.CreateToken(context.Background(), db.CreateTokenParams{
					Token:     util.CreateRandomString(10),
					ExpiresAt: time.Now().Add(-time.Hour * 24 * 7),
				})

				require.NoError(t, err)

				req, _ := http.NewRequest("POST", "/v1/tokens/validate",
					bytes.NewBufferString(fmt.Sprintf(`{"token": "%s"}`, token.Token)),
				)
				req.Header.Set("Content-Type", "application/json")
				return token, req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder, token db.Token) {
				require.Equal(t, http.StatusForbidden, rr.Code)
				require.Equal(t, `{"error":"token expired"}`, rr.Body.String())
			},
		},
		{
			name: "Disabled",
			buildRequest: func() (db.Token, *http.Request) {
				token, err := server.store.CreateToken(context.Background(), db.CreateTokenParams{
					Token:     util.CreateRandomString(10),
					ExpiresAt: time.Now().Add(time.Hour * 24 * 7),
				})

				require.NoError(t, err)

				server.store.DisableToken(context.Background(), token.ID)

				req, _ := http.NewRequest("POST", "/v1/tokens/validate",
					bytes.NewBufferString(fmt.Sprintf(`{"token": "%s"}`, token.Token)),
				)
				req.Header.Set("Content-Type", "application/json")
				return token, req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder, token db.Token) {
				require.Equal(t, http.StatusForbidden, rr.Code)
				require.Equal(t, `{"error":"token disabled"}`, rr.Body.String())
			},
		},
		{
			name: "Successfull Validation",
			buildRequest: func() (db.Token, *http.Request) {
				token, err := server.store.CreateToken(context.Background(), db.CreateTokenParams{
					Token:     util.CreateRandomString(10),
					ExpiresAt: time.Now().Add(time.Hour * 24 * 7),
				})

				require.NoError(t, err)

				req, _ := http.NewRequest("POST", "/v1/tokens/validate",
					bytes.NewBufferString(fmt.Sprintf(`{"token": "%s"}`, token.Token)),
				)
				req.Header.Set("Content-Type", "application/json")
				return token, req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder, token db.Token) {
				require.Equal(t, http.StatusOK, rr.Code)

				var respToken db.Token
				err := json.Unmarshal(rr.Body.Bytes(), &respToken)

				require.NoError(t, err)
				require.Equal(t, token.ID, respToken.ID)
				require.Equal(t, token.Token, respToken.Token)
				require.WithinDuration(t, token.ExpiresAt, respToken.ExpiresAt, time.Second)
				require.Equal(t, token.IsDisabled, respToken.IsDisabled)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			token, req := tc.buildRequest()
			tc.checkResponse(t, execRequest(t, server, req), token)
			tearDown(t, server.store, "tokens", "id", "=", token.ID)
		})
	}
}

func TestTokenList(t *testing.T) {
	server := newTestServer(t)

	testCases := []struct {
		name          string
		buildRequest  func() *http.Request
		checkResponse func(t *testing.T, rr *httptest.ResponseRecorder)
	}{
		{
			name: "UnAuthorized",
			buildRequest: func() *http.Request {
				req, _ := http.NewRequest("GET", "/v1/admin/tokens/", nil)
				return req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusUnauthorized, rr.Code)
			},
		},
		{
			name: "List",
			buildRequest: func() *http.Request {
				for i := 0; i < 10; i++ {
					_, err := server.store.CreateToken(context.Background(), db.CreateTokenParams{
						Token:     util.CreateRandomString(10),
						ExpiresAt: time.Now().Add(time.Hour * 24 * 7),
					})
					require.NoError(t, err)
				}
				req, _ := http.NewRequest("GET", "/v1/admin/tokens/", nil)
				req.SetBasicAuth("admin", server.cfg.BASIC_AUTH_PASS)
				return req
			},
			checkResponse: func(t *testing.T, rr *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, rr.Code)

				var respTokens []db.Token
				err := json.Unmarshal(rr.Body.Bytes(), &respTokens)

				require.NoError(t, err)
				require.Len(t, respTokens, 10)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.checkResponse(t, execRequest(t, server, tc.buildRequest()))
			tearDown(t, server.store, "tokens", "id", ">", 0)
		})
	}
}
