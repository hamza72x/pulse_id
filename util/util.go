package util

import (
	"encoding/json"
	"log"
	"math/rand"
	"os"
	"time"

	col "github.com/hamza72x/go-color"
)

var (
	alphaNumericChars = []rune("abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func LogFatal(err error, cause string) {
	if err != nil {
		log.Println("----------------------------")
		log.Println(col.Purple(cause))
		log.Println(col.Red(err))
		log.Println("+++++++++++++++++++++++++++++")
		os.Exit(1)
	}
}

// PrettyPrint prints a interface with all fields
func PrettyPrint(data interface{}) {
	var p []byte
	p, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("%s \n", p)
}

func CreateRandomString(limit int) string {
	charCount := int64(len(alphaNumericChars))
	b := make([]rune, limit)

	rand.Seed(time.Now().UnixNano())

	for i := range b {
		b[i] = alphaNumericChars[rand.Int63()%charCount]
	}

	return string(b)
}
